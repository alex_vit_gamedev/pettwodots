using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Profiling;
using Zenject;

namespace Engine.Mediators
{
    public class InitializeMediator : Zenject.IInitializable, IInitializeMediator, IDisposable
    {
        public event Action OnDone;

        private readonly List<ICommonInitializable> _commonInitializables;
        private readonly List<IOrderedInitializable> _orderedInitializables;
        private readonly List<IDataInitializable> _dataInitializables;

        public InitializeMediator(
            [Inject(Optional = true, Source = InjectSources.Local)]
            List<ICommonInitializable> commonInitializables,
            [Inject(Optional = true, Source = InjectSources.Local)]
            List<IOrderedInitializable> orderedInitializables,
            [Inject(Optional = true, Source = InjectSources.Local)]
            List<IDataInitializable> dataInitializables)
        {
            _commonInitializables = commonInitializables;
            _orderedInitializables = orderedInitializables;
            _dataInitializables = dataInitializables;
        }

        public void Initialize()
        {
#if UNITY_EDITOR
            Profiler.BeginSample("profiler: [InitializeMediator] Data Initializables");
#endif
            foreach (var item in _dataInitializables)
            {
                item.Initialize();
            }

#if UNITY_EDITOR
            Profiler.EndSample();
#endif

#if UNITY_EDITOR
            Profiler.BeginSample("profiler: [InitializeMediator] Order Initializables");
#endif

            var ordered = _orderedInitializables.OrderBy(x => x.InitializationOrder);
            foreach (var item in ordered)
            {
                item.Initialize();
            }

#if UNITY_EDITOR
            Profiler.EndSample();
#endif

#if UNITY_EDITOR
            Profiler.BeginSample("profiler: [InitializeMediator] Common Initializables");
#endif
            foreach (var item in _commonInitializables)
            {
                item.Initialize();
            }

#if UNITY_EDITOR
            Profiler.EndSample();
#endif
            OnDone?.Invoke();
        }

        public void Dispose()
        {
            OnDone = null;
        }
    }
}