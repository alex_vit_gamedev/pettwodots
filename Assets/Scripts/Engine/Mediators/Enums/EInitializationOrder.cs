﻿namespace Engine.Mediators
{
    public enum EInitializationOrder
    {
        SceneSwitcher = -2,
        CanvasUIProvider = 10,
        UIElementsAttacher = 11,
        WindowsController = 20,
        LevelViewController = 50,
        LevelContextProvider = 90,
        LevelLoader = 95,
        BeforePools = 98,
        Pools = 99,
        Localization = 999
    }
}