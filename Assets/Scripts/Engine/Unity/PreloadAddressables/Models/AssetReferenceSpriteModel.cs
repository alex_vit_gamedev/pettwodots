﻿using UnityEngine;

namespace Engine.Unity.PreloadAddressables.Models
{
    public class AssetReferenceSpriteModel
    {
        public Sprite Sprite;
        public bool IsDefault;
    }
}