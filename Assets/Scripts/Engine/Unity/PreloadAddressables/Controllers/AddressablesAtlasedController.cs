﻿using Engine.Unity.CoroutineManager;
using Engine.Unity.PreloadAddressables.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Engine.Unity.PreloadAddressables
{
    public class AddressablesAtlasedController : IAddressablesAtlasedController, IDisposable
    {
        private readonly IDictionary<string, AsyncOperationHandle> _handles;
        private readonly IDictionary<string, Sprite> _loaded;
        private readonly IDictionary<string, List<Action<AssetReferenceSpriteModel>>> _currentLoads;

        private static int SpawnPack = 10;

        private ICoroutineManager _coroutineManager;
        private Dictionary<AssetReferenceAtlasedSprite, Vector3> _loadBuffer;
        private bool _isWaitBuffer;
        private bool _isBufferStarted;
        private Func<Vector3> _getCameraPosition;

        // todo remove
        private static AddressablesAtlasedController _instance;

        public static AddressablesAtlasedController Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new AddressablesAtlasedController();
                return _instance;
            }
        }

        public void Initialize(ICoroutineManager coroutineManager, Func<Vector3> GetPosition)
        {
            _coroutineManager = coroutineManager;
            _getCameraPosition = GetPosition;
        }

        private Sprite _defaultSprite;
        private Sprite DefaultSprite
        {
            get
            {
                if (_defaultSprite == null)
                {
                    var defaultTexture = new Texture2D(32, 32);
                    var pixels = defaultTexture.GetPixels();
                    for (int i = 0; i < pixels.Length; i++)
                    {
                        pixels[i] = new Color(1f, 1f, 1f, 0f);
                    }

                    defaultTexture.SetPixels(pixels);
                    defaultTexture.Apply();
                    _defaultSprite = Sprite.Create(defaultTexture, new Rect(Vector2.zero, Vector2.one * defaultTexture.width), Vector2.zero);
                }
                return _defaultSprite;
            }
        }

        public AssetReferenceSpriteModel DefaultSpriteModel => new AssetReferenceSpriteModel() { Sprite = DefaultSprite, IsDefault = true };

        public AddressablesAtlasedController()
        {
            _handles = new Dictionary<string, AsyncOperationHandle>();
            _loaded = new Dictionary<string, Sprite>();
            _currentLoads = new Dictionary<string, List<Action<AssetReferenceSpriteModel>>>();
            _loadBuffer = new Dictionary<AssetReferenceAtlasedSprite, Vector3>();
            _isWaitBuffer = true;
        }

        public void Load(AssetReferenceAtlasedSprite spriteReference, Action<AssetReferenceSpriteModel> onCallback)
        {
            if (!TryAddToCurrentLoads(spriteReference, onCallback)) return;

            StartLoadSprite(spriteReference);
        }

        public void Load(AssetReferenceSprite spriteReference, Action<AssetReferenceSpriteModel> onCallback)
        {
            if (spriteReference != null)
            {
                if (!TryAddToCurrentLoads(spriteReference, onCallback)) return;

                StartLoadSprite(spriteReference);
            }
            else
                onCallback?.Invoke(DefaultSpriteModel);
        }

        public void LoadForPosition(AssetReferenceAtlasedSprite spriteReference, Vector3 position, Action<AssetReferenceSpriteModel> onCallback)
        {
            if(_coroutineManager == null)
                return;
            if (spriteReference != null)
            {
                if (!TryAddToCurrentLoads(spriteReference, onCallback)) return;

                if (!_isBufferStarted)
                {
                    _coroutineManager.StartCoroutine(StartLoadBuffer());
                }

                if (_isWaitBuffer)
                {
                    _loadBuffer.Add(spriteReference, position);
                }
                else
                {
                    StartLoadSprite(spriteReference);
                }
            }
            else
                onCallback?.Invoke(DefaultSpriteModel);
        }

        private bool TryAddToCurrentLoads(AssetReferenceAtlasedSprite spriteReference, Action<AssetReferenceSpriteModel> onCallback)
        {
            if (spriteReference == null || string.IsNullOrEmpty(spriteReference.SubObjectName))
            {
                onCallback?.Invoke(DefaultSpriteModel);
                return false;
            }

            var spriteKey = spriteReference.SubObjectName;
            if (TryReturnLoaded(spriteKey, onCallback)) return false;
            if (IsAlreadyLoaded(spriteKey, onCallback)) return false;

            onCallback?.Invoke(DefaultSpriteModel);
            AddToCurrentLoads(spriteKey, onCallback);
            return true;
        }

        private bool TryAddToCurrentLoads(AssetReferenceSprite spriteReference, Action<AssetReferenceSpriteModel> onCallback)
        {
            var spriteKey = spriteReference.SubObjectName;
            if (TryReturnLoaded(spriteKey, onCallback)) return false;
            onCallback?.Invoke(DefaultSpriteModel);
            if (IsAlreadyLoaded(spriteKey, onCallback)) return false;

            AddToCurrentLoads(spriteKey, onCallback);
            return true;
        }

        private bool TryReturnLoaded(string spriteKey, Action<AssetReferenceSpriteModel> onCallback)
        {
            if (_loaded.ContainsKey(spriteKey))
            {
                onCallback?.Invoke(new AssetReferenceSpriteModel() { Sprite = _loaded[spriteKey], IsDefault = _loaded[spriteKey] == DefaultSprite });
                return true;
            }
            return false;
        }

        private bool IsAlreadyLoaded(string spriteKey, Action<AssetReferenceSpriteModel> onCallback)
        {
            if (_currentLoads.ContainsKey(spriteKey))
            {
                AddToCurrentLoads(spriteKey, onCallback);
                return true;
            }
            return false;
        }

        private void AddToCurrentLoads(string spriteKey, Action<AssetReferenceSpriteModel> onCallback)
        {
            if (_currentLoads.ContainsKey(spriteKey))
                _currentLoads[spriteKey].Add(onCallback);
            else
                _currentLoads.Add(spriteKey, new List<Action<AssetReferenceSpriteModel>>() { onCallback });
        }

        private IEnumerator StartLoadBuffer()
        {
            _isBufferStarted = true;
            yield return null;
            //UnityEngine.Debug.LogError("Buffer count" + _loadBuffer.Count);
            _isWaitBuffer = false;
            yield return LoadBuffer();
        }

        private IEnumerator LoadBuffer()
        {
            var loadedItems = 0;
            var cameraPosition = _getCameraPosition();
            var sortedBuffer = _loadBuffer.OrderBy(x => Vector3.SqrMagnitude(x.Value - cameraPosition)).Select(x => x.Key);

            foreach (var item in sortedBuffer)
            {
                loadedItems++;
                StartLoadSprite(item);
                //  UnityEngine.Debug.LogError($"{item} => {Vector3.SqrMagnitude(_loadBuffer[item] - cameraPosition)}");
                if (loadedItems > SpawnPack)
                {
                    loadedItems = 0;
                    yield return null;
                }
            }
        }

        private void StartLoadSprite(AssetReferenceAtlasedSprite spriteReference)
        {
            var spriteKey = spriteReference.SubObjectName;
            var downloadCheck = Addressables.GetDownloadSizeAsync(spriteReference);
            if (downloadCheck.Result > 0 && downloadCheck.PercentComplete < 1f)
            {
#if UNITY_EDITOR
                Debug.LogError($"Not loaded spriteKey: {spriteKey}");
#endif
                return;
            }

            var handle = Addressables.LoadAssetAsync<Sprite>(spriteReference);
            _handles.Add(spriteKey, handle);

            handle.Completed += operationHandle =>
            {
                if (operationHandle.Status == AsyncOperationStatus.Succeeded && operationHandle.Result != null)
                {
                    _loaded[spriteKey] = operationHandle.Result;
                    OnSpriteLoaded(spriteKey, operationHandle.Result);
                }
                else
                {
#if UNITY_EDITOR
                    Debug.LogError("Can't load sprite with key " + spriteKey);
#endif
                    OnSpriteLoaded(spriteKey, DefaultSprite);
                }
            };
        }
        private void StartLoadSprite(AssetReferenceSprite spriteReference)
        {
            var spriteKey = spriteReference.SubObjectName;
            var downloadCheck = Addressables.GetDownloadSizeAsync(spriteReference);
            if (downloadCheck.Result > 0)
            {
#if UNITY_EDITOR
                Debug.LogError($"Not loaded spriteKey: {spriteKey}");
#endif
                return;
            }

            var handle = Addressables.LoadAssetAsync<Sprite>(spriteReference);
            _handles.Add(spriteKey, handle);

            handle.Completed += operationHandle =>
            {
                if (operationHandle.Status == AsyncOperationStatus.Succeeded && operationHandle.Result != null)
                {
                    _loaded[spriteKey] = operationHandle.Result;
                    OnSpriteLoaded(spriteKey, operationHandle.Result);
                }
                else
                {
#if UNITY_EDITOR
                    Debug.LogError("Can't load sprite with key " + spriteKey);
#endif
                    OnSpriteLoaded(spriteKey, DefaultSprite);
                }
            };
        }

        private void OnSpriteLoaded(string spriteKey, Sprite sprite)
        {
            if (_currentLoads.ContainsKey(spriteKey))
            {
                var isDefault = sprite == null || sprite == DefaultSprite;
                var spriteModel = new AssetReferenceSpriteModel();
                spriteModel.Sprite = isDefault ? DefaultSprite : sprite;
                spriteModel.IsDefault = isDefault;

                foreach (var callback in _currentLoads[spriteKey])
                {
                    callback?.Invoke(spriteModel);
                }
                _currentLoads[spriteKey].Clear();
            }
        }
        
        private void TryUnloadSprite(string spriteKey)
        {
            if(string.IsNullOrEmpty(spriteKey))
                return;
            if (_loaded.ContainsKey(spriteKey))
                _loaded.Remove(spriteKey);

            if (_currentLoads.ContainsKey(spriteKey))
                _currentLoads.Remove(spriteKey);

            if (_handles.ContainsKey(spriteKey))
            {
                if (_handles[spriteKey].IsValid())
                    Addressables.Release(_handles[spriteKey]);

                _handles.Remove(spriteKey);
            }
        }

        public void Unload(AssetReferenceAtlasedSprite spriteReference)
        {
            if(spriteReference == null || !spriteReference.IsValid())
                return;
            TryUnloadSprite(spriteReference.SubObjectName);
        }

        public void Unload(AssetReferenceSprite spriteReference)
        {
            if(spriteReference == null || !spriteReference.IsValid())
                return;
            TryUnloadSprite(spriteReference.SubObjectName);
        }

        public void UnloadAll()
        {
            foreach (var operationHandle in _handles)
            {
                if (operationHandle.Value.IsValid())
                    Addressables.Release(operationHandle.Value);
            }
            foreach (var loading in _currentLoads)
            {
                for (var i = loading.Value.Count - 1; i >= 0; i--)
                {
                    loading.Value[i] = null;
                }
            }
            _handles.Clear();
            _currentLoads.Clear();
            _loaded.Clear();
            _loadBuffer.Clear();
        }

        public void Dispose()
        {
            UnloadAll();
            _getCameraPosition = null;
            _instance = null;
        }
}
}