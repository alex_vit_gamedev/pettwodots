using System;
using Engine.Unity.PreloadAddressables;
using Engine.Unity.PreloadAddressables.Models;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Unity.PreloadAddressables.Views
{
    public class AddressableSpriteMaskLoaderView : MonoBehaviour
    {
        [SerializeField] private SpriteMask _spriteMask;
        [SerializeField] private AssetReferenceAtlasedSprite _atlasedSprite;
        
        private bool _isLoadingStarted;
        private Action<AssetReferenceSpriteModel> _onSpriteLoaded;

        public SpriteMask SpriteMask
        {
            get => _spriteMask;
            set => _spriteMask = value;
        }

        public AssetReferenceAtlasedSprite AtlasedSprite
        {
            get => _atlasedSprite;
            set => _atlasedSprite = value;
        }

//#if !UNITY_EDITOR
        private void Start()
        {
            _onSpriteLoaded = OnSpriteLoaded;
            if (AddressablesAtlasedController.Instance != null)
            {
                AddressablesAtlasedController.Instance.Load(AtlasedSprite, _onSpriteLoaded);
            }
        }

        private void OnSpriteLoaded(AssetReferenceSpriteModel sprite) => _spriteMask.sprite = sprite.Sprite;

        private void OnDestroy()
        {
            _spriteMask.sprite = null;
            _onSpriteLoaded = null;
            AddressablesAtlasedController.Instance.Unload(AtlasedSprite);
        }
//#endif
    }
}