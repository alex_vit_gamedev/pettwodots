﻿using System;
using Engine.Unity.PreloadAddressables;
using Engine.Unity.PreloadAddressables.Models;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Unity.PreloadAddressables.Views
{
    public class AddressableSpriteLoaderView : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _spriteRenderer;
        [SerializeField] private AssetReferenceAtlasedSprite _atlasedSprite;
        
        private bool _isOriginalLoaded;
        private Action<AssetReferenceSpriteModel> _onSpriteLoaded;

        public SpriteRenderer SpriteRenderer
        {
            get => _spriteRenderer;
            set => _spriteRenderer = value;
        }

        public AssetReferenceAtlasedSprite AtlasedSprite
        {
            get => _atlasedSprite;
            set => _atlasedSprite = value;
        }

//#if !UNITY_EDITOR
        private void Start()
        {
            _onSpriteLoaded = OnSpriteLoaded;
            if (AddressablesAtlasedController.Instance != null)
            {
                AddressablesAtlasedController.Instance.LoadForPosition(AtlasedSprite, transform.position, _onSpriteLoaded);
            }
        }

        private void OnSpriteLoaded(AssetReferenceSpriteModel sprite)
        {
            if (_isOriginalLoaded) return;

            if (SpriteRenderer != null && sprite != null)
            {
                if (!sprite.IsDefault)
                {
                    _isOriginalLoaded = true;
                    _spriteRenderer.sprite = sprite.Sprite;
                }
            }
        }

        private void OnDestroy()
        {
            _spriteRenderer.sprite = null;
            _onSpriteLoaded = null;
            AddressablesAtlasedController.Instance.Unload(AtlasedSprite);
        }
//#endif
    }
}