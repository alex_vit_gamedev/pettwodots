﻿using Engine.Unity.PreloadAddressables;
using Engine.Unity.PreloadAddressables.Models;
using System;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace Unity.PreloadAddressables.Views
{
    public class AddressableImageLoaderView : MonoBehaviour
    {
        [SerializeField] private Image _image;
        [SerializeField] private AssetReferenceAtlasedSprite _atlasedSprite;

        private bool _isSpriteLoaded = false;
        private Action<AssetReferenceSpriteModel> _onSpriteLoaded;
        
        public Image Image
        {
            get => _image;
            set => _image = value;
        }

        public AssetReferenceAtlasedSprite AtlasedSprite
        {
            get => _atlasedSprite;
            set => _atlasedSprite = value;
        }

        private void Start()
        {
            _onSpriteLoaded = SpriteLoaded;
            AddressablesAtlasedController.Instance.Load(AtlasedSprite, _onSpriteLoaded);
        }
        
        private void SpriteLoaded(AssetReferenceSpriteModel sprite)
        {
            if (_isSpriteLoaded) return;
            if (sprite == null || sprite.Sprite == null) return;

            if (!sprite.IsDefault)
            {
                _isSpriteLoaded = true;
            }
            _image.sprite = sprite.Sprite;
        }

        private void OnDestroy()
        {
            _onSpriteLoaded = null;
            _image.sprite = null;
            AddressablesAtlasedController.Instance.Unload(AtlasedSprite);
        }
    }
}