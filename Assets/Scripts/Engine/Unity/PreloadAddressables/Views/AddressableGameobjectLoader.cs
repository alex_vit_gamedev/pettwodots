using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class AddressableGameobjectLoader : MonoBehaviour
{
    [SerializeField] private List<AssetReference> _objects;
    [SerializeField] private Transform _parent;
    
    void Start()
    {
        var parent = _parent != null ? _parent : this.transform;
        foreach (var item in _objects)
        {
            item.InstantiateAsync(_parent);
        }
    }

    private void OnDestroy()
    {
        foreach (var item in _objects)
        {
            if (item.IsValid())
            {
                Addressables.Release(item);
            }
        }
    }

    [ContextMenu("ShowObjects")]
    private void  ShowObjects()
    {
        Start();
    }
}
