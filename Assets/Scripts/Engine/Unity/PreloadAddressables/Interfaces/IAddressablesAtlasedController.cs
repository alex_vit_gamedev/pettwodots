﻿using Engine.Unity.PreloadAddressables.Models;
using System;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Engine.Unity.PreloadAddressables
{
    public interface IAddressablesAtlasedController
    {
        AssetReferenceSpriteModel DefaultSpriteModel { get; }
        void Load(AssetReferenceAtlasedSprite spriteReference, Action<AssetReferenceSpriteModel> onCallback);
        void Load(AssetReferenceSprite spriteReference, Action<AssetReferenceSpriteModel> onCallback);
        void LoadForPosition(AssetReferenceAtlasedSprite spriteReference, Vector3 position, Action<AssetReferenceSpriteModel> onCallback);
        void Unload(AssetReferenceAtlasedSprite spriteReference);
        void Unload(AssetReferenceSprite spriteReference);
        void UnloadAll();
    }
}