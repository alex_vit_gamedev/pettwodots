﻿using System;
using System.Collections.Generic;

namespace Engine.Unity.PreloadAddressables
{
    public interface IAddressablesPreloader
    {
        event Action OnPreloadDone;
        event Action OnPreloadStart;
        event Action<string> OnPreloadError;
        event Action<string> OnStartLoadBundle;
        event Action<string> OnFinishLoadBundle;
        event Action<float> OnSizeCalculated;
        event Action<float> OnSizeLoaded;
        event Action<int> OnCountCalculated;
        event Action<int> OnCountLoaded;

        bool IsFirstLoading { get; }
        IReadOnlyCollection<string> LoadedLabels { get; }

        void StartPreload(IEnumerable<string> labels, float byteConverter);
        void StopPreload();
        void EnableDebug(bool value);
    }
}