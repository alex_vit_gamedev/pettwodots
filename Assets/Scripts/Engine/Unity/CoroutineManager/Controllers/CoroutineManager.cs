﻿using System;

namespace Game.Engine.UnityEvents
{
    public class CoroutineManager : AbstractCoroutineManager, IDisposable
    {
        public void Dispose()
        {
            if (_view != null)
            {
                StopAllCoroutines();
                _view.Destroy();
            }
        }
    }
}