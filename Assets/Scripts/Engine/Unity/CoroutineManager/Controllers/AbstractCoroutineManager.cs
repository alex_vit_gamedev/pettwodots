using System;
using System.Collections;
using Engine.Unity.CoroutineManager;
using UnityEngine;

namespace Game.Engine.UnityEvents
{
    public abstract class AbstractCoroutineManager : ICoroutineManager
    {
        protected CoroutineManagerView _view;

        protected AbstractCoroutineManager()
        {
            var gameObject = new GameObject("CoroutineManager");
            _view = gameObject.AddComponent<CoroutineManagerView>();
        }

        public Coroutine StartCoroutine(IEnumerator routine)
        {
            return _view.StartCoroutine(routine);
        }

        public void StopCoroutine(IEnumerator routine)
        {
            if (_view != null)
                _view.StopCoroutine(routine);
        }

        public void StopCoroutine(Coroutine routine)
        {
            if (_view != null)
                _view.StopCoroutine(routine);
        }

        public void StopAllCoroutines()
        {
            _view.StopAllCoroutines();
        }

        public Coroutine StartAfterTimeout(float timeout, Action callback)
        {
            return _view.StartCoroutine(PauseInternal(timeout, callback));
        }

        public void StartOnNextFrame(Action callback)
        {
            _view.StartCoroutine(PauseInternal(0.0f, callback));
        }

        private IEnumerator PauseInternal(float timeout, Action callback)
        {
            yield return new WaitForSeconds(timeout);
            callback?.Invoke();
        }
    }
}