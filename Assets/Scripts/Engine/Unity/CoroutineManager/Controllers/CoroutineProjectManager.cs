using UnityEngine;

namespace Game.Engine.UnityEvents
{
    public class CoroutineProjectManager : AbstractCoroutineManager, ICoroutineProjectManager
    {
        public CoroutineProjectManager()
        {
            Object.DontDestroyOnLoad(_view);
        }
    }
}