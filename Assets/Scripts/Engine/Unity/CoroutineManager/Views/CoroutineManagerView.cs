﻿using System;
using UnityEngine;

namespace Game.Engine.UnityEvents
{
    public class CoroutineManagerView : MonoBehaviour
    {
        bool _destroy = false;
        private void OnDestroy()
        {
            _destroy = true;
            StopAllCoroutines();
        }
        public void Destroy()
        {
            if (!_destroy)
            {
                StopAllCoroutines();
                Destroy(gameObject);
            }
        }
    }
}