﻿using Engine.Unity.CoroutineManager;

namespace Game.Engine.UnityEvents
{
    public interface ICoroutineProjectManager : ICoroutineManager
    {
    }
}