﻿using System;

namespace Engine.UnityEvent
{
    public interface IUnityEventMediator
    {
        event Action<float> OnUpdate;
        event Action<float> OnFixeedUpdate;
        event Action<bool> OnPause;

        void Register(IUpdatable updatable);
        void Register(IFixedUpdatable fixedUpdatable);
        void Register(IApplicationPauseListener pauseListener);

        void UnRegister(IUpdatable updatable);
        void UnRegister(IFixedUpdatable fixedUpdatable);
        void UnRegister(IApplicationPauseListener pauseListener);
    }
}