namespace Engine.UnityEvent
{
    public interface IApplicationQuitListener
    {
        void OnApplicationQuit();
    }
}