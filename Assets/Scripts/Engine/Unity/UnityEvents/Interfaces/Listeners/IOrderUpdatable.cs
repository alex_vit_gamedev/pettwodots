﻿namespace Engine.UnityEvent
{
    public interface IOrderUpdatable
    {
        int Order { get; }
        void CustomFixedUpdate(float deltaTime);
    }
}