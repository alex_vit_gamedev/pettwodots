﻿namespace Engine.UnityEvent
{
    public interface IApplicationFocusListener
    {
        void OnApplicationFocus(bool focusStatus);
    }
}
