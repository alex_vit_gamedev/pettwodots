﻿namespace Engine.UnityEvent
{
    public interface IApplicationPauseListener
    {
        void OnApplicationPause(bool isPaused);
    }
}
