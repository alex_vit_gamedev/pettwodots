﻿using System;

namespace Engine.UnityEvent
{
    public interface IUnityEventFixedUpdate
    {
        void StartAfterTimeout(float time, Action OnAction);
        public void StopAll();
    }
}