﻿using System.Collections.Generic;
using Zenject;

namespace Engine.UnityEvent
{
    public class UnityEventSceneMediator : UnityEventMediator
    {
        public UnityEventSceneMediator(
            [Inject(Optional = true, Source = InjectSources.Local)] List<IOrderUpdatable> orderUpdatables,
            [Inject(Optional = true, Source = InjectSources.Local)] List<IUpdatable> updatables,
            [Inject(Optional = true, Source = InjectSources.Local)] List<ILateUpdatable> lateUpdatables,
            [Inject(Optional = true, Source = InjectSources.Local)] List<IFixedUpdatable> fixedUpdatables,
            [Inject(Optional = true, Source = InjectSources.Local)] List<IApplicationPauseListener> applicationPauses,
            [Inject(Optional = true, Source = InjectSources.Local)] List<IApplicationFocusListener> applicationFocus,
            [Inject(Optional = true, Source = InjectSources.Local)] List<IApplicationQuitListener> applicationQuits)
            : base(orderUpdatables, updatables, lateUpdatables, fixedUpdatables, applicationPauses, applicationFocus, applicationQuits)
        {
        }
    }
}