﻿using System;
using System.Collections.Generic;

namespace Engine.UnityEvent
{
    public class UnityEventRegisterMediator : IUnityEventMediator, IUpdatable, IFixedUpdatable, IApplicationPauseListener, IDisposable
    {
        public event Action<float> OnUpdate;
        public event Action<float> OnFixeedUpdate;
        public event Action<bool> OnPause;

        private readonly List<IUpdatable> _updatables;
        private readonly List<IFixedUpdatable> _fixedUpdatables;
        private readonly List<IApplicationPauseListener> _pauseListeners;

        public UnityEventRegisterMediator()
        {
            _updatables = new List<IUpdatable>();
            _fixedUpdatables = new List<IFixedUpdatable>();
            _pauseListeners = new List<IApplicationPauseListener>();
        }

        public void Register(IUpdatable updatable)
        {
            if (!_updatables.Contains(updatable))
                _updatables.Add(updatable);
        }

        public void Register(IFixedUpdatable fixedUpdatable)
        {
            if (!_fixedUpdatables.Contains(fixedUpdatable))
                _fixedUpdatables.Add(fixedUpdatable);
        }

        public void Register(IApplicationPauseListener pauseListener)
        {
            if (!_pauseListeners.Contains(pauseListener))
                _pauseListeners.Add(pauseListener);
        }

        public void UnRegister(IApplicationPauseListener pauseListener)
        {
            if (_pauseListeners.Contains(pauseListener))
                _pauseListeners.Remove(pauseListener);
        }

        public void UnRegister(IUpdatable updatable)
        {
            if (_updatables.Contains(updatable))
                _updatables.Remove(updatable);
        }

        public void UnRegister(IFixedUpdatable fixedUpdatable)
        {
            if (_fixedUpdatables.Contains(fixedUpdatable))
                _fixedUpdatables.Remove(fixedUpdatable);
        }

        public void CustomUpdate(float deltaTime)
        {
            for (int i = 0; i < _updatables.Count; i++)
                _updatables[i].CustomUpdate(deltaTime);

            OnUpdate?.Invoke(deltaTime);
        }

        public void CustomFixedUpdate(float deltaTime)
        {
            for (int i = 0; i < _fixedUpdatables.Count; i++)
                _fixedUpdatables[i].CustomFixedUpdate(deltaTime);

            OnFixeedUpdate?.Invoke(deltaTime);
        }

        public void OnApplicationPause(bool isPaused)
        {
            for (int i = 0; i < _pauseListeners.Count; i++)
                _pauseListeners[i].OnApplicationPause(isPaused);

            OnPause?.Invoke(isPaused);
        }

        public void Dispose()
        {
            OnUpdate = null;
            OnFixeedUpdate = null;
            OnPause = null;

            _updatables.Clear();
            _fixedUpdatables.Clear();
            _pauseListeners.Clear();
        }
    }
}