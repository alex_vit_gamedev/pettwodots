using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
namespace Engine.UnityEvent
{
    public class UnityEventFixedUpdate : IUnityEventFixedUpdate, IOrderUpdatable, IDisposable
    {
        public int Order => 5;
        private class ActionTimeData
        {
            public Action Action;
            public float Time;
        }
        private List<ActionTimeData> _actionTime;
        public UnityEventFixedUpdate()
        {
            _actionTime = new List<ActionTimeData>();

        }
        public void StartAfterTimeout(float time, Action OnAction)
        {
            _actionTime.Add(new ActionTimeData { Action = OnAction, Time = time });
        }
        public void StopAll()
        {
            _actionTime.Clear();
        }

        public void CustomFixedUpdate(float deltaTime)
        {
            foreach (var data in _actionTime)
            {
                data.Time -= deltaTime;
            }

            var finishActionTime = _actionTime.Where(w => w.Time <= 0);
            if (finishActionTime != null)
                foreach (var data in finishActionTime.ToArray())
                {
                    data.Action.Invoke();
                    _actionTime.Remove(data);
                }
        }

        public void Dispose()
        {
            StopAll();
        }
    }
}
