﻿using System.Collections.Generic;

namespace Engine.UnityEvent
{
    public class UnityEventProjectMediator : UnityEventMediator
    {
        public UnityEventProjectMediator(List<IOrderUpdatable> orderUpdatables, List<IUpdatable> updatables, List<ILateUpdatable> lateUpdatables,
            List<IFixedUpdatable> fixedUpdatables, List<IApplicationPauseListener> applicationPauses,
            List<IApplicationFocusListener> applicationFocus, List<IApplicationQuitListener> applicationQuits)
            : base(orderUpdatables, updatables, lateUpdatables, fixedUpdatables, applicationPauses, applicationFocus, applicationQuits)
        {
            _unityEventMediatorView.SetDontDestroy();
        }
    }
}