﻿
using System;
using System.Collections;

namespace Game.Engine.CheckInternetConnection
{
    public interface ICheckInternetConnection
    {
        event Action<bool> OnChecked;
        IEnumerator TryOuterCheckInternet();
        void ForceCheckConnection();
        bool IsInternetConnection();
    }
}