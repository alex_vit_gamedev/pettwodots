using System;
using System.Collections;
using Engine.UnityEvent;
using Game.Engine.UnityEvents;
using UnityEngine;
using UnityEngine.Networking;

namespace Game.Engine.CheckInternetConnection
{
    public class CheckInternetConnection : ICheckInternetConnection, IApplicationPauseListener, IApplicationFocusListener, IDisposable
    {
        public event Action<bool> OnChecked;

        private const float SecTimeCheckConnection = 60f;
        private const string EchoServer = "https://google.com";

        private readonly ICoroutineProjectManager _coroutineProjectManager;
        private bool _isInternetConnection;
        private readonly Coroutine _checkConnectionCoroutine;
        private Coroutine _runCheckConnection;
        private float _lastCheckInternetConnection;

        public CheckInternetConnection(ICoroutineProjectManager coroutineProjectManager)
        {
            _coroutineProjectManager = coroutineProjectManager;
            _isInternetConnection = false;
            _lastCheckInternetConnection = Time.time;
            _checkConnectionCoroutine = _coroutineProjectManager.StartCoroutine(TimeCheckInternet());
        }

        public void OnApplicationPause(bool isPaused)
        {
            if (!isPaused)
            {
                RunCheckInternetConnection();
            }
        }

        public void OnApplicationFocus(bool focusStatus)
        {
            if (focusStatus)
            {
                RunCheckInternetConnection();
            }
        }

        public IEnumerator TryOuterCheckInternet()
        {
            if(_runCheckConnection != null)
                yield break;
            _lastCheckInternetConnection = Time.time + SecTimeCheckConnection;
            _runCheckConnection = _coroutineProjectManager.StartCoroutine(CheckInternet());
            yield return _runCheckConnection;
        }

        private IEnumerator TimeCheckInternet()
        {
            while (true)
            {
                var lastTime = _lastCheckInternetConnection - Time.time;
                if (lastTime <= 0)
                {
                    RunCheckInternetConnection();
                    yield return new WaitForSeconds(SecTimeCheckConnection);
                }
                else
                {
                    yield return new WaitForSeconds(lastTime);
                }
            }
        }

        private void RunCheckInternetConnection()
        {
            if (_runCheckConnection == null)
            {
                _lastCheckInternetConnection = Time.time + SecTimeCheckConnection;
                _runCheckConnection = _coroutineProjectManager.StartCoroutine(CheckInternet());
            }
        }

        private IEnumerator CheckInternet()
        {
            using (var request = new UnityWebRequest(EchoServer))
            {
                request.timeout = 5;
                yield return request.SendWebRequest();
                _isInternetConnection = !request.isNetworkError && request.responseCode != 404;
                OnChecked?.Invoke(_isInternetConnection);
            }
            _runCheckConnection = null;
        }

        public bool IsInternetConnection()
        {
            return _isInternetConnection;
        }

        public void ForceCheckConnection()
        {
            RunCheckInternetConnection();
        }

        public void Dispose()
        {
            if (_checkConnectionCoroutine != null)
                _coroutineProjectManager.StopCoroutine(_checkConnectionCoroutine);
            if (_runCheckConnection != null)
                _coroutineProjectManager.StopCoroutine(_runCheckConnection);

            OnChecked = null;
        }
    }
}