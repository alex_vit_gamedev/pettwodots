﻿using System;
using Object = UnityEngine.Object;

namespace Engine.Unity.UnityResourceProviders
{
    public interface IUnityResourceProvider
    {
        T LoadAsset<T>(string assetPath) where T : Object;
        T[] LoadAssets<T>(string assetsPath) where T : Object;

        void LoadAssetAsync<T>(string assetPath, Action<T> onLoaded) where T : Object;
        
    }
}