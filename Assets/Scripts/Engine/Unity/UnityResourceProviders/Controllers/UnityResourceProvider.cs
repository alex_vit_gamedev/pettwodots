﻿using System;
using System.Collections;
using Game.Engine.UnityEvents;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Engine.Unity.UnityResourceProviders
{
    public class UnityResourceProvider : IUnityResourceProvider
    {
        private readonly ICoroutineProjectManager _coroutineManager;

        public UnityResourceProvider(ICoroutineProjectManager coroutineManager)
        {
            _coroutineManager = coroutineManager;
        }

        public T LoadAsset<T>(string assetPath) where T : Object
        {
            var resource = Resources.Load<T>(assetPath);
            if (resource == null)
            {
#if UNITY_EDITOR
                Debug.LogError($"({typeof(T)}){assetPath} was not found.");
#endif
            }
            return resource;
        }

        public T[] LoadAssets<T>(string assetsPath) where T : Object
        {
            return Resources.LoadAll<T>(assetsPath);
        }

        public void LoadAssetAsync<T>(string assetPath, Action<T> onLoaded) where T : Object
        {
            _coroutineManager.StartCoroutine(LoadAsyncAsset(assetPath, onLoaded));
        }

        private IEnumerator LoadAsyncAsset<T>(string path, Action<T> onLoaded) where T : Object
        {
            var asyncOperation = Resources.LoadAsync<T>(path);
            while (!asyncOperation.isDone)
            {
                yield return null;
            }
            onLoaded?.Invoke(asyncOperation.asset as T);
        }
    }
}