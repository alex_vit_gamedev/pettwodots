﻿using System;

namespace Game.Common.ApplicationVersion.Common
{
    public interface IApplicationVersionProvider
    {
        Version Version { get; set; }
    }
}
