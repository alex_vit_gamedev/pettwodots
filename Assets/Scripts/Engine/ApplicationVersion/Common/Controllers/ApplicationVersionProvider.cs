﻿using System;
using UnityEngine;

namespace Game.Common.ApplicationVersion.Common
{
    public class ApplicationVersionProvider : IApplicationVersionProvider
    {
        public Version Version { get; set; }

        public ApplicationVersionProvider()
        {
            Version = Version.Parse(Application.version);
        }
    }
}
