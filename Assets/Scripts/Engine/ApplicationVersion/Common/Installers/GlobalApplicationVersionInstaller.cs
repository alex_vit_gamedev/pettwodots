﻿using UnityEngine;
using Zenject;

namespace Game.Common.ApplicationVersion.Common
{
    [CreateAssetMenu(menuName = "ScriptableObject/Installers/Global/ApplicationVersion/Global/VersionProvider", fileName = "GlobalApplicationVersionInstaller")]
    public class GlobalApplicationVersionInstaller : ScriptableObjectInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesTo<ApplicationVersionProvider>().AsSingle();
        }
    }
}
