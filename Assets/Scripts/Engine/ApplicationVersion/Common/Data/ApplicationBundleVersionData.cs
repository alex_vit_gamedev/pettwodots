﻿using System;

namespace Game.Common.ApplicationVersion.Preloader
{
    [Serializable]
    public class ApplicationBundleVersionData
    {
        public int BundleVersion;

        public ApplicationBundleVersionData(int bundleVersion)
        {
            BundleVersion = bundleVersion;
        }
    }
}