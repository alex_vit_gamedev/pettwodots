using UnityEngine;

namespace Game.Common.Configuration
{
    public class PrefsSaveLoadConfigManager : ISaveLoadConfigManager
    {
        public void Set(string key, string content)
        {
            PlayerPrefs.SetString(key, content);
        }

        public string Get(string key)
        {
            return PlayerPrefs.GetString(key, string.Empty);
        }

        public bool HasKey(string key)
        {
            return PlayerPrefs.HasKey(key);
        }

        public void Save()
        {
            
        }

        public void Load()
        {
            
        }

        public void Clear()
        {
            
        }
    }
}