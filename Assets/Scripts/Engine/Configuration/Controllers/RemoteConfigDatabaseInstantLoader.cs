﻿using System.Collections.Generic;
using Engine.Mediators;

namespace Game.Common.Configuration
{
    public class RemoteConfigDatabaseInstantLoader : IDataInitializable
    {
        private static bool _initialized;

        private readonly IRemoteConfigFetchController _configDatabase;
        private readonly List<IAfterConfigDatabaseInitializable> _initializables;

        public RemoteConfigDatabaseInstantLoader(IRemoteConfigFetchController configDatabase,
                                                 List<IAfterConfigDatabaseInitializable> initializables)
        {
            _configDatabase = configDatabase;
            _initializables = initializables;
        }

        public void Initialize()
        {
            if (_configDatabase.IsConfigsFetched()) return;
            if (_initialized) return;

            _initialized = true;
            foreach (var entity in _initializables)
                entity.Initialize();
        }
    }
}
