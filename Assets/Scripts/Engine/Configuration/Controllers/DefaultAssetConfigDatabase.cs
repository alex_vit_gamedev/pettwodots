﻿using System.Linq;
using UnityEngine;

namespace Game.Common.Configuration
{
    public class DefaultAssetConfigDatabase : IConfigDatabase
    {
        private readonly ConfigDatabaseAsset _configDatabaseAsset;
        private readonly IConfigDatabaseTextSerializer _configDatabaseTextSerializer;

        public DefaultAssetConfigDatabase(ConfigDatabaseAsset configDatabaseAsset,
                                        IConfigDatabaseTextSerializer configDatabaseTextSerializer)
        {
            _configDatabaseAsset = configDatabaseAsset;
            _configDatabaseTextSerializer = configDatabaseTextSerializer;
        }

        public T GetConfig<T>(EConfigType configType)
        {
            var defaultConfigAsset = _configDatabaseAsset.DefaultConfigs.FirstOrDefault(c => c.ConfigType == configType);
            if (defaultConfigAsset != null && defaultConfigAsset.ConfigAsset != null && !string.IsNullOrEmpty(defaultConfigAsset.ConfigAsset.text))
            {
                if (_configDatabaseTextSerializer.TryDeserialize<T>(defaultConfigAsset.ConfigAsset.text, out var config))
                {
                    return config;
                }
            }
#if UNITY_EDITOR
            Debug.LogError($"{this}: not found config '{configType}' or serialization error. Returned default or null");
#endif
            return default;
        }
    }
}