﻿using Zenject;

namespace Game.Common.Configuration
{
    public class ConfigDatabaseChainBuilder : IFactory<IConfigDatabase>
    {
        private readonly ConfigDatabaseAsset _configDatabaseAsset;
        private readonly IConfigDatabaseTextSerializer _configDatabaseTextSerializer;
        private readonly IRemoteConfigFetchController _remoteConfigDatabase;
        private readonly IRemoteConfigFetchController _remoteConfigFetchController;
        private readonly ISaveLoadConfigManager _saveLoadConfigManager;

        public ConfigDatabaseChainBuilder(ConfigDatabaseAsset configDatabaseAsset,
                                          IConfigDatabaseTextSerializer configDatabaseTextSerializer,
                                          IRemoteConfigFetchController remoteConfigDatabase,
                                          IRemoteConfigFetchController remoteConfigFetchController,
                                          ISaveLoadConfigManager saveLoadConfigManager)
        {
            _configDatabaseAsset = configDatabaseAsset;
            _configDatabaseTextSerializer = configDatabaseTextSerializer;
            _remoteConfigDatabase = remoteConfigDatabase;
            _remoteConfigFetchController = remoteConfigFetchController;
            _saveLoadConfigManager = saveLoadConfigManager;
        }

        public IConfigDatabase Create()
        {
            var defaultAssetConfigDatabase = new DefaultAssetConfigDatabase(_configDatabaseAsset, _configDatabaseTextSerializer);
            var localConfigDatabase = new LocalConfigDatabase(defaultAssetConfigDatabase, _remoteConfigDatabase, _configDatabaseTextSerializer, _saveLoadConfigManager);
            var remoteConfigDatabase = new RemoteConfigDatabase(localConfigDatabase, _remoteConfigFetchController, _configDatabaseTextSerializer);
            var overridenConfigDatabase = new OverridedConfigDatabase(remoteConfigDatabase, _configDatabaseAsset, _configDatabaseTextSerializer);

            return overridenConfigDatabase;
        }
    }
}