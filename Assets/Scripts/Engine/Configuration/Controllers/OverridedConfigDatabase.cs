﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Common.Configuration
{
    public class OverridedConfigDatabase : IConfigDatabase
    {
        private readonly IConfigDatabase _configDatabase;
        private readonly IConfigDatabaseTextSerializer _configDatabaseTextSerializer;
        private readonly IDictionary<string, string> _overrideConfigs;

        public OverridedConfigDatabase(IConfigDatabase configDatabase,
                                                ConfigDatabaseAsset configsData,
                                                IConfigDatabaseTextSerializer configDatabaseTextSerializer)
        {
            _configDatabase = configDatabase;
            _configDatabaseTextSerializer = configDatabaseTextSerializer;
            _overrideConfigs = configsData.OverridenConfigs.ToDictionary(k => k.ConfigType.ToString(), v => v.ConfigAsset.text);
        }

        public T GetConfig<T>(EConfigType configType)
        {
            var configTypeString = configType.ToString();

            if (_overrideConfigs.ContainsKey(configTypeString))
            {
#if UNITY_EDITOR
                Debug.LogError("ALERT!! Exist ovveride config: " + configType);
#endif
                var text = _overrideConfigs[configTypeString];

                if (_configDatabaseTextSerializer.TryDeserialize<T>(text, out var config))
                {
                    return config;
                }
#if UNITY_EDITOR
                Debug.LogError($"{this}: '{configType}' serialization error.");
#endif
            }

            return _configDatabase.GetConfig<T>(configType);
        }
    }
}