﻿using System;
using UnityEngine;

namespace Game.Common.Configuration
{
    public class RemoteConfigDatabaseInitOrderConflictReporter : IDisposable
    {
        private readonly IRemoteConfigFetchController _configDatabase;

        public RemoteConfigDatabaseInitOrderConflictReporter(IRemoteConfigFetchController configDatabase)
        {
            _configDatabase = configDatabase;

            _configDatabase.OnInitOrderConflict += OnInitOrderConflict;
        }

        private void OnInitOrderConflict(string configType)
        {
#if UNITY_EDITOR
            Debug.LogError($"RemoteConfigDatabase: config '{configType}' was requested before database initialized.");
#endif
        }

        public void Dispose()
        {
            _configDatabase.OnInitOrderConflict -= OnInitOrderConflict;
        }
    }
}
