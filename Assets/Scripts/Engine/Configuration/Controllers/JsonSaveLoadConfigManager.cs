using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;

namespace Game.Common.Configuration
{
    public class JsonSaveLoadConfigManager : ISaveLoadConfigManager
    {
        private readonly IDictionary<string, string> _cachedContentDict;
        private const string _configFileName = "Json/configs.json";

        private string _filePath;

        private bool _isDirty;

        public JsonSaveLoadConfigManager()
        {
            _cachedContentDict = new Dictionary<string, string>();
            _filePath = Path.Combine(Application.persistentDataPath, _configFileName);
        }
        
        public void Load()
        {
            if(!File.Exists(_filePath))
                return;
            var jsonStr = File.ReadAllText(_filePath);
            var resultDict = JsonConvert.DeserializeObject<Dictionary<string,string>>(jsonStr);
            foreach (var pair in resultDict)
            {
                if(string.IsNullOrEmpty(pair.Key) || string.IsNullOrEmpty(pair.Value))
                    continue;
                _cachedContentDict[pair.Key] = pair.Value;
            }
            _isDirty = false;
        }
        
        public void Save()
        {
            if(!_isDirty)
                return;
            var jsonStr = JsonConvert.SerializeObject(_cachedContentDict, Formatting.Indented);
            if (!Directory.Exists(Application.persistentDataPath + "/Json/"))
                Directory.CreateDirectory(Application.persistentDataPath + "/Json/");
            File.WriteAllText(_filePath, jsonStr);
            _isDirty = false;
        }

        public void Clear()
        {
            _isDirty = false;
            _cachedContentDict?.Clear();
        }

        public void Set(string key, string content)
        {
            _cachedContentDict[key] = content;
            _isDirty = true;
        }
        
        public string Get(string key)
        {
            if (_cachedContentDict.TryGetValue(key, out var result))
                return result;
            return string.Empty;
        }

        public bool HasKey(string key) => _cachedContentDict.ContainsKey(key);
    }
}