﻿namespace Game.Common.Configuration
{
    public class RemoteConfigDatabase : IConfigDatabase
    {
        private readonly IConfigDatabaseTextSerializer _configSerializer;
        private readonly IConfigDatabase _configDatabase;
        private readonly IRemoteConfigFetchController _remoteConfigFetchController;
        
        public RemoteConfigDatabase(IConfigDatabase configDatabase,
                                    IRemoteConfigFetchController remoteConfigFetchController,
                                    IConfigDatabaseTextSerializer configSerializer)
        {
            _configSerializer = configSerializer;
            _configDatabase = configDatabase;
            _remoteConfigFetchController = remoteConfigFetchController;
        }

        public T GetConfig<T>(EConfigType configType)
        {
                if (_remoteConfigFetchController.Configs.TryGetValue(configType.ToString(), out var value))
                {
                    var configText = value as string;
                    if (!string.IsNullOrEmpty(configText) && _configSerializer.TryDeserialize<T>(configText, out var config))
                    {
                        return config;
                    }
                }
            return _configDatabase.GetConfig<T>(configType);
        }
    }
}