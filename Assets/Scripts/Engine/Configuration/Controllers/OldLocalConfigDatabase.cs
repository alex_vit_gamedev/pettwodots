﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Common.Configuration
{
    public class OldLocalConfigDatabase : IConfigDatabase
    {
        private readonly IConfigDatabaseAsset _database;
        private readonly IConfigDatabaseTextSerializer _configSerializer;

        public OldLocalConfigDatabase(IConfigDatabaseAsset configDatabase, IConfigDatabaseTextSerializer configSerializer)
        {
            _database = configDatabase;
            _configSerializer = configSerializer;
        }

        public T GetConfig<T>(EConfigType configType)
        {
            var text = TryFindConfig(configType, _database.OverridenConfigs) ?? TryFindConfig(configType, _database.DefaultConfigs);
            if (string.IsNullOrEmpty(text))
            {
#if UNITY_EDITOR
                Debug.LogError($"LocalConfigDatabase: Config '{configType}' was not found.");
#endif
                return default;
            }

            if (!_configSerializer.TryDeserialize<T>(text, out var config))
            {
#if UNITY_EDITOR
                Debug.LogError($"LocalConfigDatabase: Config '{configType}' serialization error.");
#endif
            }

            return config;
        }

        public void GetConfigAsync<T>(EConfigType configType, Action<T> onDone)
        {
            var config = GetConfig<T>(configType);
            onDone?.Invoke(config);
        }

        private string TryFindConfig(EConfigType configType, IReadOnlyList<ConfigTextAssetData> configs)
        {
            var config = configs.FirstOrDefault(c => c.ConfigType == configType);
            if (config.ConfigAsset == null) return null;
            if (string.IsNullOrEmpty(config.ConfigAsset.text)) return null;

            return config.ConfigAsset.text;
        }
    }   
}
