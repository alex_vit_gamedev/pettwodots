﻿using System;
using Newtonsoft.Json;
using UnityEngine;

namespace Game.Common.Configuration
{
    public class NewtonsoftConfigSerializer : IConfigDatabaseTextSerializer
    {
        private readonly DateTimeSerializerSettings _dateTimeSerializer;

        public NewtonsoftConfigSerializer()
        {
            _dateTimeSerializer = new DateTimeSerializerSettings();
        }

        public bool TryDeserialize<T>(string source, out T result)
        {
            result = default;

            try
            {
                var obj = JsonConvert.DeserializeObject<T>(source, _dateTimeSerializer);
                result = obj;
                return true;
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }

            return false;
        }
    }
}
