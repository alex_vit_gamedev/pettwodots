﻿using UnityEngine;

namespace Game.Common.Configuration
{
    public class LocalConfigDatabase : IConfigDatabase
    {
        private readonly IConfigDatabase _configDatabase;
        private readonly IRemoteConfigFetchController _remoteConfigDatabase;
        private readonly IConfigDatabaseTextSerializer _configDatabaseTextSerializer;
        private readonly ISaveLoadConfigManager _saveLoadConfigManager;

        public LocalConfigDatabase(IConfigDatabase configDatabase,
                                   IRemoteConfigFetchController remoteConfigDatabase,
                                   IConfigDatabaseTextSerializer configDatabaseTextSerializer,
                                   ISaveLoadConfigManager saveLoadConfigManager)
        {
            _configDatabase = configDatabase;
            _remoteConfigDatabase = remoteConfigDatabase;
            _configDatabaseTextSerializer = configDatabaseTextSerializer;
            _saveLoadConfigManager = saveLoadConfigManager;
        }

        public T GetConfig<T>(EConfigType configType)
        {
                var configTypeString = configType.ToString();

                if (_remoteConfigDatabase.IsConfigsFetched(configType.ToString()) && 
                    _saveLoadConfigManager.HasKey(configTypeString))
                {
                    var text = _saveLoadConfigManager.Get(configTypeString);
                    if (_configDatabaseTextSerializer.TryDeserialize<T>(text, out var config))
                    {
                        return config;
                    }
#if UNITY_EDITOR
                    Debug.LogError($"{this}: '{configType}' serialization error.");
#endif
                }

                return _configDatabase.GetConfig<T>(configType);
        }
    }
}