using System;
using UnityEngine;

namespace Game.Common.Configuration
{
    [Serializable]
    public class ConfigTextAssetData
    {
        public EConfigType ConfigType => _configType;
        public TextAsset ConfigAsset => _configAsset;

        [SerializeField] private EConfigType _configType;
        [SerializeField] private TextAsset _configAsset;
    }
}