﻿namespace Game.Common.Configuration
{
    public interface IConfigDatabase
    {
        T GetConfig<T>(EConfigType configType);
    }
}