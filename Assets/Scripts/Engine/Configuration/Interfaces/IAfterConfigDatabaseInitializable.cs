﻿namespace Game.Common.Configuration
{
    public interface IAfterConfigDatabaseInitializable
    {
        void Initialize();
    }
}
