﻿using System.Collections.Generic;

namespace Game.Common.Configuration
{
    public interface IConfigDatabaseAsset
    {
        IReadOnlyList<ConfigTextAssetData> DefaultConfigs { get; }
        IReadOnlyList<ConfigTextAssetData> OverridenConfigs { get; }
    }
}
