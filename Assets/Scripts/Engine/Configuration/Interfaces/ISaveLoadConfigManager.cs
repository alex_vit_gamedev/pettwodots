namespace Game.Common.Configuration
{
    public interface ISaveLoadConfigManager
    {
        void Load();
        void Set(string key, string content);
        void Save();
        void Clear();
        string Get(string key);
        bool HasKey(string key);
    }
}