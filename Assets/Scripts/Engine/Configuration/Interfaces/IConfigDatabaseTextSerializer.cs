﻿namespace Game.Common.Configuration
{
    public interface IConfigDatabaseTextSerializer
    {
        bool TryDeserialize<T>(string source, out T result);
    }
}
