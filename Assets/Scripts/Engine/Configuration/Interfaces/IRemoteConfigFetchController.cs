﻿using System;
using System.Collections.Generic;

namespace Game.Common.Configuration
{
    public interface IRemoteConfigFetchController
    {
        IReadOnlyDictionary<string, object> Configs { get; }

        event Action<string> OnInitOrderConflict;
        event Action OnStartLoadConfigs;
        void FetchConfigs(Action onDone);
        void ForceStopFetch();
        bool IsConfigsFetched(string configType);
        bool IsConfigsFetched();
    }
}
