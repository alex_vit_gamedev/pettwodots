﻿using UnityEngine;
using Zenject;

namespace Game.Common.Configuration
{
    [CreateAssetMenu(menuName = "ScriptableObject/Installers/Common/RemoteConfigDatabaseInstantLoaderInstaller", fileName = "CommonRemoteConfigDatabaseInstantLoaderInstaller")]
    public class CommonRemoteConfigDatabaseInstantLoaderInstaller : ScriptableObjectInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesTo<RemoteConfigDatabaseInstantLoader>().AsSingle();
        }
    }
}
