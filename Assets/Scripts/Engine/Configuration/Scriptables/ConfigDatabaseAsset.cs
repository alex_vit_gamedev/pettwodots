﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.Common.Configuration
{
    [CreateAssetMenu(menuName = "ScriptableObject/Database/ConfigDatabase", fileName = "ConfigDatabase")]
    public class ConfigDatabaseAsset : ScriptableObject, IConfigDatabaseAsset
    {
        public IReadOnlyList<ConfigTextAssetData> DefaultConfigs => _defaultConfigs;
        public IReadOnlyList<ConfigTextAssetData> OverridenConfigs => _ovveridenConfigs;

        [SerializeField] private List<ConfigTextAssetData> _defaultConfigs;
        [SerializeField] private List<ConfigTextAssetData> _ovveridenConfigs;
    }
}