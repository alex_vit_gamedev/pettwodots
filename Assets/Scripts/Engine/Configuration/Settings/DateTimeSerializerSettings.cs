﻿using Newtonsoft.Json;

namespace Game.Common.Configuration
{
	public class DateTimeSerializerSettings : JsonSerializerSettings
	{
		public DateTimeSerializerSettings()
		{
			DateFormatString = "MM/dd/yyyy HH:mm";
		}
	}
}