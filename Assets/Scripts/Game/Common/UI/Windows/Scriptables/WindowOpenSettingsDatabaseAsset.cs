using System.Collections.Generic;
using UnityEngine;

namespace Game.Common.UI.Windows
{
    [CreateAssetMenu(menuName = "ScriptableObject/Database/Settings/WindowOpenSettingsDatabaseAsset", fileName = "WindowOpenSettingsDatabaseAsset")]
    public class WindowOpenSettingsDatabaseAsset : ScriptableObject
    {
        public WindowSettingsData[] WindowSettings => _windowSettings;
        public IReadOnlyList<EWindowType> WindowPopups => _windowPopups;

        [SerializeField] private WindowSettingsData[] _windowSettings;
        [SerializeField] private List<EWindowType> _windowPopups;
    }
}