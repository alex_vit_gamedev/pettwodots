namespace Game.Common.UI.Windows
{
    public interface IWindowStorage
    {
        void Add(IWindow window);
        IWindow Get(EWindowType windowType);
    }
}