namespace Game.Common.UI.Windows
{
    public interface IWindowController
    {
        EWindowType Current { get; }
        bool IsWindowSwitching { get; }
        
        void OpenSingle(EWindowType windowType);
        void OpenReplace(EWindowType windowType);
        void OpenEnqueue(EWindowType windowType);

        void BackOnce();
        void BackFull();
    }
}