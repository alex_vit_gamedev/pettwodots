using System;
using UnityEngine;

namespace Game.Common.UI.Windows
{
    public interface IWindowView
    {
        void Show(EWindowType windowType, Action onShowed);
        void Hide(EWindowType windowType, Action onHidden);
        void SetParent(Transform parent);
    }
}