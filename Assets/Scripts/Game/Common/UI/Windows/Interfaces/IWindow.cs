using Game.Common.UI.Windows.Events;

namespace Game.Common.UI.Windows
{
    public interface IWindow
    {
        EWindowType WindowType { get; }

        void WindowStartOpening(IWindowActionListener listener, EWindowType fromOpen);
        void WindowStartClosing(IWindowActionListener listener, EWindowType toOpen);
    }
}