namespace Game.Common.UI.Windows
{
    public interface IWindowOpenSettingsDataProvider
    {
        WindowSettingsData Get(EWindowType windowType);
        bool IsPopupWindow(EWindowType windowType);
    }
}