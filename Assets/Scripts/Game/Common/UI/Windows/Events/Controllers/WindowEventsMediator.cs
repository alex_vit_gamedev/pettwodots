using System;
using System.Collections.Generic;

namespace Game.Common.UI.Windows.Events
{
    public class WindowEventsMediator : IDisposable
    {
        private readonly List<IBeforeWindowSwitchListener> _beforeListeners;
        private readonly List<IAfterWindowSwitchListener> _afterListeners;
        
        public WindowEventsMediator(IWindowEventPublisher eventPublisher,
            List<IBeforeWindowSwitchListener> beforeListeners, List<IAfterWindowSwitchListener> afterListeners)
        {
            _beforeListeners = beforeListeners;
            _afterListeners = afterListeners;

            eventPublisher.ListenToBeforeSwitch(ListenToBeforeSwitch);
            eventPublisher.ListenToAfterSwitch(ListenToAfterSwitch);
        }
        
        private void ListenToBeforeSwitch(EWindowType newWindow, EWindowType oldWindow)
        {
            foreach (var item in _beforeListeners)
                item.OnBeforeWindowSwitch(newWindow, oldWindow);
        }

        private void ListenToAfterSwitch(EWindowType newWindow, EWindowType oldWindow)
        {
            foreach (var item in _afterListeners)
                item.OnAfterWindowSwitch(newWindow, oldWindow);
        }

        public void Dispose()
        {
            _beforeListeners?.Clear();
            _afterListeners?.Clear();
        }
    }
}