namespace Game.Common.UI.Windows.Events
{
    public interface IAfterWindowSwitchListener
    {
        void OnAfterWindowSwitch(EWindowType newWindow, EWindowType oldWindow);
    }
}