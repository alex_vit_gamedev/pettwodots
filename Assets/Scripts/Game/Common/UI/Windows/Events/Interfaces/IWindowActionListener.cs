namespace Game.Common.UI.Windows.Events
{
    public interface IWindowActionListener
    {
        void OnOpened(IWindow window);
        void OnClosed(IWindow window);
        void OnBack(IWindow window);
    }
}