using System;

namespace Game.Common.UI.Windows.Events
{
    public interface IWindowEventPublisher
    {
        void ListenToBeforeSwitch(Action<EWindowType, EWindowType> onBefore);
        void ListenToAfterSwitch(Action<EWindowType, EWindowType> onAfter);
    }
}