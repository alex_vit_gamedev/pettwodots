namespace Game.Common.UI.Windows.Events
{
    public interface IBeforeWindowSwitchListener
    {
        void OnBeforeWindowSwitch(EWindowType newWindow, EWindowType oldWindow);
    }
}