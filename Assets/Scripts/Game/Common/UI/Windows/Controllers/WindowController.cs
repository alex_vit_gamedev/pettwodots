using System;
using System.Collections.Generic;
using Engine.Mediators;
using Game.Common.UI.Windows.Events;

namespace Game.Common.UI.Windows
{
    public partial class WindowController : IWindowController, IWindowEventPublisher, IWindowActionListener, IOrderedInitializable, IDisposable
    {
        public EWindowType Current { get; private set; }
        public EInitializationOrder InitializationOrder => EInitializationOrder.WindowsController;
        public bool IsWindowSwitching => _openingWindow != null || _closingWindow != null;

        private readonly Stack<IWindow> _windowStack;
        
        private readonly IWindowStorage _windowsStorage;
        private readonly IUiController _uiController;
        private readonly IWindowOpenSettingsDataProvider _windowSettingsProvider;

        private IWindow _popupWindow;
        private IWindow _openingWindow;
        private IWindow _closingWindow;
        
        private Action<EWindowType, EWindowType> _beforeSwitchListener;
        private Action<EWindowType, EWindowType> _afterSwitchListener;
        private SafetyModule _module;
        private SafetyModule _moduleFront;
        private bool _initialized;

        public WindowController(IWindowStorage windowsStorage, IUiController uiController, IWindowOpenSettingsDataProvider windowSettingsProvider)
        {
            _windowStack = new Stack<IWindow>();
            _windowsStorage = windowsStorage;
            _uiController = uiController;
            _windowSettingsProvider = windowSettingsProvider;
            
            _module = new SafetyModule(this);
            _moduleFront = new SafetyModule(this);
        }
        
        public void Initialize()
        {
            _uiController.Register(this);

            Current = EWindowType.Default;
            _openingWindow = _windowsStorage.Get(EWindowType.Default);
            _windowStack.Push(_openingWindow);
            _module.InvokeListener(_beforeSwitchListener, EWindowType.Default, EWindowType.None);
            _openingWindow.WindowStartOpening(this, Current);
            _initialized = true;
        }

        public void BackOnce()
        {
            if (_popupWindow != null)
            {
                if (_moduleFront.CheckCanFrontBack() == false) return;

                _closingWindow = _popupWindow;
                _openingWindow = _windowStack.Peek();
                _moduleFront.InvokeListener(_beforeSwitchListener, _openingWindow.WindowType, _closingWindow.WindowType);
                _moduleFront.CloseWindow();
            }
            else
            {
                if (_module.CheckCanBack() == false) return;

                _closingWindow = _windowStack.Pop();
                _openingWindow = _windowStack.Peek();
                _module.InvokeListener(_beforeSwitchListener, _openingWindow.WindowType, _closingWindow.WindowType);
                _module.CloseWindow();
            }
        }

        public void BackFull()
        {
            if (_module.CheckCanBack() == false) return;
            _closingWindow = _windowStack.Pop();

            while (_windowStack.Count > 1)
                _windowStack.Pop();
            _openingWindow = _windowStack.Peek();
            _module.InvokeListener(_beforeSwitchListener, _openingWindow.WindowType, _closingWindow.WindowType);
            _module.CloseWindow();
        }

        public void OpenEnqueue(EWindowType windowType)
        {
            if (_module.CheckCanOpenWindow(windowType) == false) return;

            _closingWindow = _windowStack.Peek();
            _openingWindow = _windowsStorage.Get(windowType);
            _windowStack.Push(_openingWindow);
            _module.InvokeListener(_beforeSwitchListener, _openingWindow.WindowType, _closingWindow.WindowType);
            _module.CloseWindow();

        }

        public void OpenReplace(EWindowType windowType)
        {
            if (_module.CheckCanOpenWindow(windowType) == false) return;

            _closingWindow = _windowStack.Count > 1 ? _windowStack.Pop() : _windowStack.Peek();
            _openingWindow = _windowsStorage.Get(windowType);
            _windowStack.Push(_openingWindow);
            _module.InvokeListener(_beforeSwitchListener, _openingWindow.WindowType, _closingWindow.WindowType);
            _module.CloseWindow();
        }

        public void OpenSingle(EWindowType windowType)
        {
            if (!_windowSettingsProvider.IsPopupWindow(windowType))
            {
                if (_module.CheckCanOpenWindow(windowType) == false) return;

                _closingWindow = _windowStack.Peek();
                while (_windowStack.Count > 1)
                    _windowStack.Pop();
                _openingWindow = _windowsStorage.Get(windowType);
                _windowStack.Push(_openingWindow);
                _module.InvokeListener(_beforeSwitchListener, _openingWindow.WindowType, _closingWindow.WindowType);
                _module.CloseWindow();
            }
            else
            {
                if (_moduleFront.CheckCanOpenWindowFront(windowType) == false) return;

                _openingWindow = _windowsStorage.Get(windowType);

                _popupWindow = _openingWindow;
                _moduleFront.InvokeListener(_beforeSwitchListener, _openingWindow.WindowType, EWindowType.None);

                _moduleFront.OpenWindow();
            }
        }

        void IWindowActionListener.OnOpened(IWindow window)
        {
            if (window != null && _openingWindow == window)
            {
                var opened = _openingWindow.WindowType;
                var closed = _closingWindow == null ? EWindowType.None : _closingWindow.WindowType;
                _openingWindow = null;
                _closingWindow = null;

                if (_windowSettingsProvider.IsPopupWindow(opened))
                {
                    _moduleFront.InvokeListener(_afterSwitchListener, opened, closed);
                }
                else
                {
                    _module.InvokeListener(_afterSwitchListener, opened, closed);
                }
            }
        }

        void IWindowActionListener.OnClosed(IWindow window)
        {
            var isPopup = _popupWindow != null;

            if (window != null && _closingWindow == window)
            {
                _popupWindow = null;

                if (_openingWindow != null)
                {
                    Current = _openingWindow.WindowType;

                    if (!isPopup)
                        _module.OpenWindow();
                    else
                    {
                        _openingWindow = null;
                        _closingWindow = null;
                    }
                }
                else
                {
                    _closingWindow = null;
                }
            }
        }

        void IWindowActionListener.OnBack(IWindow window)
        {
            BackOnce();
        }

        void IWindowEventPublisher.ListenToBeforeSwitch(Action<EWindowType, EWindowType> beforeSwitch)
        {
            _beforeSwitchListener = beforeSwitch;
        }

        void IWindowEventPublisher.ListenToAfterSwitch(Action<EWindowType, EWindowType> afterSwitch)
        {
            _afterSwitchListener = afterSwitch;
        }

        public void Dispose()
        {
            _windowStack?.Clear();
            _beforeSwitchListener = null;
            _beforeSwitchListener = null;
            _openingWindow = null;
            _closingWindow = null;
            _module.Dispose();
            _moduleFront.Dispose();
            _module = null;
            _moduleFront = null;
        }
    }
}