using System;
using Game.Common.UI.Windows.Events;
using UnityEngine;

namespace Game.Common.UI.Windows
{
    partial class WindowController
    {
        private class SafetyModule
        {
            private WindowController _windowController;

            public SafetyModule(WindowController windowController)
            {
                _windowController = windowController;
            }
            
            public bool CheckCanOpenWindow(EWindowType newWindow)
            {
                if (CheckNotInitialized()) return false;
                if (CheckAlreadySwitching()) return false;
                if (CheckNewWindowInvalid(newWindow)) return false;

                return true;
            }
            public bool CheckCanOpenWindowFront(EWindowType newWindow)
            {
                if (CheckNotInitialized()) return false;
                
                return true;
            }

            public bool CheckCanBack()
            {
                if (CheckNotInitialized()) return false;
                if (CheckAlreadySwitching()) return false;
                if (CheckStackEmpty()) return false;

                return true;
            }
            public bool CheckCanFrontBack()
            {
                if (CheckNotInitialized()) return false;

                return true;
            }

            public void InvokeListener(Action<EWindowType, EWindowType> listener, EWindowType newWindow, EWindowType oldWindow)
            {
                try
                {
                    listener?.Invoke(newWindow, oldWindow);
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }

            public void CloseWindow()
            {
                try
                {
                    if (_windowController._openingWindow != null)
                    {
                        _windowController._closingWindow?.WindowStartClosing(_windowController, _windowController._openingWindow.WindowType);    
                    }
                    else
                    {
                        _windowController._closingWindow?.WindowStartClosing(_windowController, EWindowType.None);
                    }
                    
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                    (_windowController as IWindowActionListener).OnClosed(_windowController._closingWindow);
                }
            }

            public void OpenWindow()
            {
                try
                {
                    if (_windowController._closingWindow == null)
                    {
                        _windowController._openingWindow.WindowStartOpening(_windowController, EWindowType.None);
                    }
                    else
                    {
                        _windowController._openingWindow.WindowStartOpening(_windowController, _windowController._closingWindow.WindowType);    
                    }
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                    (_windowController as IWindowActionListener).OnOpened(_windowController._openingWindow);
                }
            }

            private bool CheckAlreadySwitching()
            {
                if (_windowController._openingWindow != null || _windowController._closingWindow != null)
                {
                    return true;
                }
                return false;
            }

            private bool CheckNewWindowInvalid(EWindowType newWindow)
            {
                if (newWindow == EWindowType.Default)
                {
#if UNITY_EDITOR
                    Debug.LogError("WindowsController: opening default window manually is not allowed.");
#endif
                    return true;
                }
                if (_windowController.Current == newWindow)
                {
#if UNITY_EDITOR
                    Debug.LogError($"WindowsController: '{newWindow}' already opened.");
#endif
                    return true;
                }
                return false;
            }

            private bool CheckStackEmpty()
            {
                if (_windowController._windowStack.Count <= 1)
                {
#if UNITY_EDITOR
                    Debug.LogError("WindowsController: can not back, stack is empty.");
#endif
                    return true;
                }
                return false;
            }
            
            private bool CheckStackFrontEmpty()
            {
                if (_windowController._popupWindow == null)
                {
#if UNITY_EDITOR
                    Debug.LogError("WindowsController: can not back, stack is empty.");
#endif
                    return true;
                }
                return false;
            }

            private bool CheckNotInitialized()
            {
                if (!_windowController._initialized)
                {
#if UNITY_EDITOR
                    Debug.LogError("WindowsController: tried to perform operation before controller initialized");
#endif
                    return true;
                }

                return false;
            }

            public void Dispose()
            {
                _windowController = null;
            }
        }
    }
}