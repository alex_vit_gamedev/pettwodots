using System;
using UnityEngine;

namespace Game.Common.UI.Windows
{
    public class AbstractWindowView : MonoBehaviour, IWindowView
    {
        public void Show(EWindowType windowType, Action onShowed)
        {
            gameObject.SetActive(true);
            onShowed?.Invoke();
        }

        public void Hide(EWindowType windowType, Action onHidden)
        {
            gameObject.SetActive(false);
            onHidden?.Invoke();
        }

        public void SetParent(Transform parent)
        {
            transform.SetParent(parent, false);
        }
    }
}