using Game.Common.UI.Windows.Events;
using UnityEngine;

namespace Game.Common.UI.Windows
{
    public class AbstractWindow : IWindow
    {
        public EWindowType WindowType { get; }
        
        private readonly IWindowView _view;
        private IWindowActionListener _windowListener;
        private Transform _parent;

        public AbstractWindow(IWindowView view)
        {
            _view = view;
        }

        public void SetParent(Transform parent)
        {
            _parent = parent;
            _view?.SetParent(parent);
        }
        
        public void WindowStartOpening(IWindowActionListener listener, EWindowType fromOpen)
        {
            OnBeforeOpen();
            _windowListener = listener;
            _view.Show(fromOpen, OnShowed);
        }

        public void WindowStartClosing(IWindowActionListener listener, EWindowType toOpen)
        {
            OnBeforeClose();
            _view.Hide(toOpen, OnHidden);
        }
        
        private void OnShowed()
        {
            _windowListener.OnOpened(this);
            OnAfterOpen();
        }

        private void OnHidden()
        {
            var temp = _windowListener;
            _windowListener = null;
            temp.OnClosed(this);
            OnAfterClose();
        }
        
        protected virtual void OnBeforeOpen()
        {

        }

        protected virtual void OnAfterOpen()
        {

        }

        protected virtual void OnBeforeClose()
        {

        }

        protected virtual void OnAfterClose()
        {

        }

        protected void Back()
        {
            _windowListener?.OnBack(this);
        }
    }
}