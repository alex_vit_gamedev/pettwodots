using System;
using System.Collections.Generic;
using System.Linq;

namespace Game.Common.UI.Windows
{
    public class WindowOpenSettingsDataProvider : IWindowOpenSettingsDataProvider, IDisposable
    {
        private readonly Dictionary<EWindowType, WindowSettingsData> _windowSettingsDict;
        private readonly WindowOpenSettingsDatabaseAsset _windowDatabaseAsset;


        public WindowOpenSettingsDataProvider(WindowOpenSettingsDatabaseAsset databaseAsset)
        {
            _windowDatabaseAsset = databaseAsset;
            _windowSettingsDict = databaseAsset.WindowSettings.ToDictionary(x => x.WindowType);
        }
        
        public WindowSettingsData Get(EWindowType windowType)
        {
            if (!_windowSettingsDict.TryGetValue(windowType, out var setting))
            {
#if UNITY_EDITOR
               UnityEngine.Debug.LogError($"WindowsOpenSettingsDatabase: '{windowType}' was not found.");
#endif                
            }
            return setting;
        }

        public bool IsPopupWindow(EWindowType windowType)
        {
            if (_windowDatabaseAsset.WindowPopups.Count == 0)
            {
                return false;
            } 
            return _windowDatabaseAsset.WindowPopups.Contains(windowType);
        }

        public void Dispose()
        {
            _windowSettingsDict?.Clear();
        }
    }
}