using System;

namespace Game.Common.UI.Windows
{
    [Serializable]
    public class WindowSettingsData
    {
        public EWindowType WindowType;
        public bool IsShowBlocker;
        public bool CloseByTap;
    }
}