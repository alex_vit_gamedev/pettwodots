using System;
using System.Collections.Generic;

namespace Game.Common.UI.Windows
{
    public class WindowStorage : IWindowStorage, IDisposable
    {
        private readonly Dictionary<EWindowType, IWindow> _windowsDict;

        public WindowStorage(List<IWindow> windows)
        {
            _windowsDict = new Dictionary<EWindowType, IWindow>();
           
            foreach (var window in windows)
                _windowsDict.Add(window.WindowType, window);
        }

        public void Add(IWindow window)
        {
            _windowsDict.Add(window.WindowType, window);
        }

        public IWindow Get(EWindowType windowType)
        {
            if (!_windowsDict.TryGetValue(windowType, out var window))
            {
#if UNITY_EDITOR
                UnityEngine.Debug.LogError($"WindowsStorage: window '{windowType}' was not found.");
#endif                
            }

            return window;
        }

        public void Dispose()
        {
            _windowsDict?.Clear();
        }
    }
}