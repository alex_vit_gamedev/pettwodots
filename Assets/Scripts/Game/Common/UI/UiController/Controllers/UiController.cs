using Game.Common.UI.Windows;

namespace Game.Common.UI.UiController
{
    public class UiController : IUiController
    {
        public EWindowType Current => _windowsController == null ? EWindowType.Default : _windowsController.Current;

        public bool IsWindowSwitching => _windowsController.IsWindowSwitching;

        private IWindowController _windowsController;

        public void OpenEnqueue(EWindowType windowType) => _windowsController?.OpenEnqueue(windowType);
        public void OpenSingle(EWindowType windowType) => _windowsController?.OpenSingle(windowType);
        public void OpenReplace(EWindowType windowType) => _windowsController?.OpenReplace(windowType);

        public void BackOnce() => _windowsController?.BackOnce();
        public void BackFull() => _windowsController?.BackFull();
        public void Register(IWindowController windowsController) => _windowsController = windowsController;
    }
}