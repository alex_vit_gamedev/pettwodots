using Game.Common.UI.Windows;

namespace Game.Common.UI
{
    public interface IUiController : IWindowController
    {
        void Register(IWindowController windowController);
    }
}