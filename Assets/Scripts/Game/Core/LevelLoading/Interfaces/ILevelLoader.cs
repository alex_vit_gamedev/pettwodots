using System;

namespace Game.Core.LevelLoading
{
    public interface ILevelLoader
    {
        public event Action OnLevelReady;

        public void LoadLevel();
        public void LoadLevel(string levelName);
    }
}