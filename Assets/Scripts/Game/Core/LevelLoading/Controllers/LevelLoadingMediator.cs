using System;
using Engine.Mediators;
using Game.Core.Level;

namespace Game.Core.LevelLoading
{
    public class LevelLoadingMediator : ILevelLoader, IOrderedInitializable, IDisposable
    {
        public event Action OnLevelReady;
        public EInitializationOrder InitializationOrder => EInitializationOrder.LevelLoader;

        private readonly ILevelContextProvider _levelContextProvider;
        private LevelData _levelData;

        public LevelLoadingMediator(ILevelContextProvider levelContextProvider)
        {
            
        }

        public void Initialize()
        {
            _levelData = null;
        }

        public void LoadLevel()
        {
            
        }

        public void LoadLevel(string levelName)
        {
            
        }

        private void LoadLevelProcess()
        {
            
        }
        
        public void Dispose()
        {
            OnLevelReady = null;
        }
    }
}