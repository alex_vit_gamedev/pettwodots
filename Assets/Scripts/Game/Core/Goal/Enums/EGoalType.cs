namespace Game.Core.Goal
{
    public enum EGoalType
    {
        None = 0,
        Dot_Red = 1,
        Dot_Green = 2,
        Dot_Blue = 3,
        Dot_Cyan = 4,
        Dot_Magenta = 5,
        Dot_Yellow = 6
    }
}