using System;

namespace Game.Core.Goal
{
    [Serializable]
    public class GoalData
    {
        public EGoalType GoalType;
        public int Amount;
    }
}