using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Core.Blocks
{
    public class BlockModel : IBlockModel
    {
        public event Action<IBlockModelBehaviour> OnModuleDamaged;
        public event Action<IBlockModel> OnDestroy;
        public event Action<IBlockModel, Vector2Int> OnPositionChanged;
        public event Action<IBlockModelBehaviour> OnBehaviourRemoved;
        
        public IBlockModelBehaviour Child { get; set; }
        public EBlockType CurrentType => _current.CurrentType;
        public EDotType DotType => _current.DotType;
        public int TotalHealth => _current.TotalHealth;
        public IBlockModelBehaviour Current => _current;

        public Vector2Int Position
        {
            get => _position;
            set {
                if (_position == value) return;
                var oldPosition = _position;
                _position = value;
                OnPositionChanged?.Invoke(this, oldPosition);
            }
        }

        private Vector2Int _position;
        private List<IBlockModelBehaviour> _behaviours;
        private IBlockModelBehaviour _current;

        public BlockModel(EBlockType module, EDotType dotType, Vector2Int position, int health)
        {
            _current = new BlockModelBehaviour(module, dotType, health);
            _behaviours = new List<IBlockModelBehaviour>();
            Position = position;
        }
        
        public void AddBehaviour(IBlockModelBehaviour behaviour)
        {
            behaviour.Child = _current;
            _current = behaviour;
            _behaviours.Add(behaviour);
        }

        public void RemoveBehaviour(IBlockModelBehaviour behaviour)
        {
            if (behaviour == _current)
            {
                _current = behaviour.Child;
            }
            behaviour.Child = null;
            _behaviours.Remove(behaviour);
            OnBehaviourRemoved?.Invoke(behaviour);
        }

        public void Destroy()
        {
            OnDestroy?.Invoke(this);
            OnModuleDamaged = null;
            OnBehaviourRemoved = null;
            OnDestroy = null;
            OnPositionChanged = null;
        }
    }
}