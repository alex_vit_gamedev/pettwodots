using System;

namespace Game.Core.Blocks
{
    public class BlockModelBehaviour : IBlockModelBehaviour
    {
        public event Action<IBlockModelBehaviour> OnModuleDamaged;
        public IBlockModelBehaviour Child { get; set; }
        public EBlockType CurrentType { get; }
        public EDotType DotType { get; set; }
        public int TotalHealth { get; private set; }
        
        public BlockModelBehaviour(EBlockType module, EDotType dotType, int health)
        {
            DotType = dotType;
            CurrentType = module;
            TotalHealth = health;
        }
    }
}