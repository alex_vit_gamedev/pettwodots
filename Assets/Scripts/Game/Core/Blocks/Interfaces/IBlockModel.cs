using System;
using UnityEngine;

namespace Game.Core.Blocks
{
    public interface IBlockModel : IBlockModelBehaviour
    {
        event Action<IBlockModel> OnDestroy;
        event Action<IBlockModel, Vector2Int> OnPositionChanged;

        IBlockModelBehaviour Current { get; }
        Vector2Int Position { get; }

        void AddBehaviour(IBlockModelBehaviour behaviour);
        void RemoveBehaviour(IBlockModelBehaviour behaviour);
        void Destroy();
    }
}