using System;

namespace Game.Core.Blocks
{
    public interface IBlockModelBehaviour
    {
        event Action<IBlockModelBehaviour> OnModuleDamaged;
        IBlockModelBehaviour Child { get; set; }
        EBlockType CurrentType { get; }
        EDotType DotType { get; }
        int TotalHealth { get; }
    }
}