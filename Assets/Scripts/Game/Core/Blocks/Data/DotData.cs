using System;
using UnityEngine;

namespace Game.Core.Blocks
{
    [Serializable]
    public class DotData
    {
        public Vector2Int Position;
        public EDotType DotColor;
        public bool IsRandomlyColored;
    }
}