namespace Game.Core.Blocks
{
    public enum EDotType
    {
        None = -1,
        Clear = 0,
        Red = 1,
        Green = 2,
        Blue = 3,
        Cyan = 4,
        Magenta = 5,
        Yellow = 6
    }
}