using System;
using System.Collections.Generic;
using Game.Core.Blocks;
using Game.Core.Goal;

namespace Game.Core.Level
{
    [Serializable]
    public class LevelData
    {
        public List<GoalData> GoalData;
        public List<DotData> DotData;
        public int MovesCount;
    }
}