namespace Game.Core.Level
{
    public class LevelDataProvider : ILevelDataProvider
    {
        private readonly ILevelDataSerializer _levelDataSerializer;
        
        public LevelDataProvider(ILevelDataSerializer levelDataSerializer)
        {
            _levelDataSerializer = levelDataSerializer;
        }
        
        public LevelData LoadData(string levelName)
        {
            var json = LoadDataFromJson(levelName);
            if (string.IsNullOrEmpty(json))
                return null;
            return _levelDataSerializer.Deserialize(levelName);
        }

        private string LoadDataFromJson(string levelName)
        {
            //load level data from json (from remote or local storage)
            return null;
        }
    }
}