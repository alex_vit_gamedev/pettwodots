namespace Game.Core.Level
{
    public class LevelContextProvider : ILevelContextProvider
    {
        private readonly ILevelDataProvider _levelDataProvider;
        private readonly ILevelNameProvider _levelNameProvider;
        
        public LevelContextProvider(ILevelDataProvider levelDataProvider, ILevelNameProvider levelNameProvider)
        {
            _levelDataProvider = levelDataProvider;
        }

        public LevelData GetLevelData() => _levelDataProvider.LoadData(GetLevelName());

        public int GetLevelNumber() => _levelNameProvider.GeLevelNumber();
        private string GetLevelName() => _levelNameProvider.GetLevelName();
    }
}