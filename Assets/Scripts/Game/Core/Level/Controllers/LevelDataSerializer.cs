using System;
using Newtonsoft.Json;
using UnityEngine;

namespace Game.Core.Level
{
    public class LevelDataSerializer : ILevelDataSerializer
    {
        public string Serialize(LevelData levelData) => JsonConvert.SerializeObject(levelData);

        public LevelData Deserialize(string text)
        {
            try
            {
                var levelData = JsonConvert.DeserializeObject<LevelData>(text);
                return levelData;
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }
            return null;
        }
    }
}