namespace Game.Core.Level
{
    public interface ILevelNameProvider
    {
        string GetLevelName();
        int GeLevelNumber();
    }
}