namespace Game.Core.Level
{
    public interface ILevelDataSerializer
    {
        public string Serialize(LevelData levelData);
        public LevelData Deserialize(string jsonData);
    }
}