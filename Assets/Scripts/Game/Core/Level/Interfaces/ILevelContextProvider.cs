namespace Game.Core.Level
{
    public interface ILevelContextProvider
    {
        LevelData GetLevelData();
        int GetLevelNumber();
    }
}