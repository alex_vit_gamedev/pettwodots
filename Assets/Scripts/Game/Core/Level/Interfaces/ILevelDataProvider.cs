namespace Game.Core.Level
{
    public interface ILevelDataProvider
    {
        public LevelData LoadData(string levelName);
    }
}